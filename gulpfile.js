// set source and dest dirs for scss compilation
var scss_src 	= ['scss/*.scss', '!scss/_*.scss'];
var scss_dest	= './public/stylesheets/theme/';

// set ports for browserSync
var bs = {
	port: {
		ui    : 30477, // main port for viewing site
		debug : 30478, // port to view browserSync admin panel
		weinre: 30479 // port for browserSync to open weinre debugger (open source debug tool like chrome devtools/firebug)
	}
};

// declare gulp and plugins here
var gulp 		= require('gulp'),
	browserSync = require('browser-sync'),
	plumber 	= require('gulp-plumber'),
	debug 		= require('gulp-debug'),
	notify 		= require('gulp-notify'),
	gutil 		= require('gulp-util');

// new shit from webpack config guide
// https://words.mxbry.com/making-react-webpack-browsersync-gulp-play-nice-and-hot-reload-b2c1e01522e3#.pw5sm5h83
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');

var path = require("path");
var fs   = require('fs');

var pkg = JSON.parse(fs.readFileSync('./build-config.json'));

var webpack         = require('webpack');
var webpackSettings = require('./webpack.config.js');

var bundler = webpack(webpackSettings);

/**
 * Wait for sass, then launch the server
 */
gulp.task('webpack-dev', function() {
	browserSync(
			{
			server: {
				baseDir: [pkg.paths.build],

				middleware: [
					webpackDevMiddleware(bundler, {
						publicPath: webpackSettings.output.publicPath,
						stats: { colors: true }
					}),
					webpackHotMiddleware(bundler)
				]
			},
		files: [
			pkg.build.css + '**/*.css',
			pkg.paths.build + '**/*.html'
			],
		port: bs.port.ui,
		open: false,
		ui: {
			port: bs.port.debug,
			weinre: {
				port: bs.port.weinre,
			}
		},
		notify: {
			styles: {
				top: 			'auto',
				bottom: 		'0',
				borderRadius: 	'5px 5px 0 0',
				background: 	'rgba(27,32,50,0.7)',
			}
		}
	});
});


// /**
//  * Compile files from _scss into both _site/css (for live injecting)
//  */
// gulp.task('sass', function(){
// 	var source = gulp.src(scss_src)
// 	.pipe(plumber())
// 	.pipe(notify({
// 		title: "Sass",
// 		message: "Sass init."}))
// 	.pipe(sourcemaps.init()) // Start Sourcemaps
// 	.pipe(sass({
// 		// set output format for compiled css
// 		outputStyle: 'expanded',
// 		indentType: 'tab'
// 		}).on('error', sass.logError))
// 	.on('error', gutil.log);

// 	var pipeMaps = source.pipe(clone())
// 	.pipe(sourcemaps.write('maps'));

// 	var pipeMinify = source.pipe(clone())
// 	.pipe(rename({suffix: '.min'}))
// 	.pipe(cssnano({ // minify with cssnano
// 		convertValues: {
// 			length: false
// 		},
// 		discardComments: {
// 			removeAll: true
// 		},
// 		discardUnused: {
// 			// do not minify font-face rule
// 			// (bug: cssnano completely deletes the rule instead of minifying)
// 			fontFace: false
// 		}
// 	}))
// 	.pipe(clip());

// 	return merge(pipeMaps, pipeMinify)
// 	.pipe(gulp.dest(scss_dest))
// 	.pipe(debug({title: 'sass-merge:'}))
// 	.pipe(notify({
// 		title: "Sass",
// 		message: "Sass complete. File changed: <%= file.relative %>"}));
// });

/**
 * Default task, running just `gulp` will compile the sass,
 * compile the jekyll site, launch BrowserSync & watch files.
 */
 gulp.task('default', ['webpack-dev'], function(){
 });
