var path = require("path");
var fs   = require('fs');

/**
 * Webpack config file for development
 *
 * @author Dominic Gan <dominicgangx@gmail.com>
 */
var pkg = JSON.parse(fs.readFileSync('./build-config.json'));

var environments = require('gulp-environments');
var development  = environments.development;
var production   = environments.production;

var webpack      = require('webpack');
var autoprefixer = require('autoprefixer');
var precss       = require('precss');

module.exports = {
	entry: {
		app: [
		'webpack/hot/dev-server',
		'webpack-hot-middleware/client',
		path.join(process.cwd(), pkg.src.js, 'app.jsx')
		],
	},
	output: {
		path: path.join(process.cwd(), pkg.dev.js),
		publicPath: '/js/',
		filename: '[name].js'
	},
	plugins: [
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.NoErrorsPlugin(),
		new webpack.HotModuleReplacementPlugin()
	],
	resolve: {
		extensions: ['', '.js', '.jsx']
	},
	debug: environments.development(),
	module: {
		loaders: [
		{
			// jsx loader
			test: /\.jsx?$/,
			exclude: /(node_modules|bower_components)/,
			include: [path.join(process.cwd(), pkg.src.js)],
			loaders: [
			'react-hot',
			'babel?presets[]=react,presets[]=es2015,presets[]=stage-1',
			'webpack-module-hot-accept'
			],
		},
		{
			// image loader
			test: /\.(jpe?g|png|gif|svg)$/i,
			exclude: /(node_modules|bower_components)/,
			include: [path.join(process.cwd(), pkg.src.img)],
			loaders: [
			'file?hash=sha512&digest=hex&name=[hash].[ext]',
			'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
			]
		},
		{
			test: /\.css$/,
			loaders: [
				"style-loader",
				"css-loader?importLoaders=1",
				"postcss-loader",
			]
		},
		{
			test: /\.scss$/,
			loaders: [
				"style-loader",
				"css-loader?importLoaders=1",
				"postcss-loader",
				"sass-loader"
			]
		}
		]
	},
	postcss: function(){
		return [autoprefixer, precss];
	},
	imageWebpackLoader: {
		mozjpeg: {
			quality: 65
		},
		pngquant:{
			quality: "65-90",
			speed: 4
		},
		svgo:{
			plugins: [
			{
				removeViewBox: false
			},
			{
				removeEmptyAttrs: false
			}
			]
		}
	}
};
