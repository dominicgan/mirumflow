# MirumSG/front-end-coding-test
---

## Initialize

1. Clone repo
2. Install dependencies `npm install`
3. Start webpack dev server `gulp`
4. Navigate to browsersync url

## Build

1. Run `webpack -p --config webpack.production.config.js`
2. To view build:
  
    * navigate to `dist/` folder
    * start http server `http-server -p <your port>`
    * navigate to server url