import React from 'react';

/**
 * Contact Form Component
 */
 export default class ContactForm extends React.Component {
 	constructor(props) {
 		super(props);
 		this.state = {
 			value: '',
 			submitted: false,
 			isFilled: false,
 			isValidEmail: false,
 			msgType: '',
 			msgText: undefined
 		};

 		this.handleChange = this.handleChange.bind(this);
 		this.handleSubmit = this.handleSubmit.bind(this);
 	}
 	componentDidUpdate(){
 		if (this.state.submitted) {
	 		if (!this.state.isFilled){
	 		this.validateEmptyField();
	 		} else if (this.state.isFilled && !this.state.isValidEmail) {
	 		this.validateEmailFormat();
	 		} else if (this.state.isFilled && this.state.isValidEmail){
	 			this.sendEmail();
	 		}
 		} 
 	}
 	handleChange(event) {
 		this.setState({value: event.target.value});
 	}
 	handleSubmit(event) {
 		event.preventDefault();
 		this.setState({
 			isFilled: false,
 			isValidEmail: false,
 			submitted: true
 		})
 	}
 	sendEmail = () => {
		this.printMessage('success', 'Thank you for subscribing!');
 		this.setState({
 			submitted: false
 		})
		// todo: add POST request to send form
	}
	validateEmptyField = () => {
		if (this.state.value.length > 0 && this.state.submitted){
			this.setState({
				isFilled: true
			});
		} else {
			this.printMessage('error', 'Please fill in an email address.');
			this.setState({
				isFilled: false,
				submitted: false
			});
			return false;
		}
	}
	validateEmailFormat = () => {
		if (this.state.isFilled && this.state.submitted){
			let emailAddress = this.state.value;
		    // regex pattern adapted from:
		    // http://stackoverflow.com/questions/8398403/jquery-javascript-to-check-if-correct-e-mail-was-entered
		    const pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
		    if (pattern.test(emailAddress)){
				this.setState({
					isValidEmail: true
				});
		    } else {
				this.printMessage('error', 'This is not a valid email address. Please try again');
				this.setState({
					isValidEmail: false,
					submitted: false
				});
				return false;
		    }
		}
	}
	printMessage = (type, message) => {
		// todo: add print message
		console.log(type, message);
		this.setState({
			msgType: 'form--'+type,
			msgText: message
		})
	}
	render(){
		return (
			<form className="form" id="contact-form" onSubmit={this.handleSubmit}>
			<input type="text" placeholder="Write e-mail address" value={this.state.value} onChange={this.handleChange} className="form__inputtext" ref="email_input"/>
			<div className={"form__message " + (this.state.msgType.length ? this.state.msgType : '')}>{this.state.msgText}</div>
			<input type="submit" value="Subscribe" className="form__submit"/>
			</form>
			)
	}
};