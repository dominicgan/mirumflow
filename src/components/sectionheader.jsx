import React from 'react';

/**
 * Section header (heading + subtitle)
 */
export default class SectionHeader extends React.Component {
	createSubtitleTemp(subtitle){
		return {__html: subtitle}
	};
 	subtitleText(subtitle){
 		if (subtitle !== false) {
 			return (
 				<p className="section__subtitle" dangerouslySetInnerHTML={this.createSubtitleTemp(subtitle)} /> // temp hack. to fix
 				);
 		} else {
 			return false;
 		}
 	};
 	render(){
 		var header = this.props.header;
 		var subtitle = this.props.subtitle ? this.props.subtitle : false;
 		return (
 			<header>
 			<h1 className="section__title">{header}</h1>
 			{this.subtitleText(subtitle)}
 			</header>
 			)
 	}
};