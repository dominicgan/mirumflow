import React from 'react';
import $ from 'jquery';

/**
 * How it works slider component
 *
 * guides:
 * http://tech.oyster.com/using-react-and-jquery-together/
 * http://codepen.io/zuraizm/pen/vGDHl
 * http://codepen.io/brian-baum/pen/MwxorO.babel
 */
export default class HowWorksSlider extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            slidePos: 0,
            sliderInitialized: false,
            sliderActive: false
        };
    }
    componentDidMount(){
        if (this.props.data){
            this.initSlider();
        }
    }
    componentDidUpdate(){
        if (!this.state.sliderInitialized){
        this.initSlider();
        }
    }
    initSlider(){
        // elements
        var slidesContainer = $(this.refs.howworks__slides);
        var sliderEl        = $(this.refs.howworks__slidercontainer);
        var slideEl         = slidesContainer.children('.howworks__slide');
        var slideLftBtn     = $(this.refs.howworks__lftbtn);
        var slideRgtBtn     = $(this.refs.howworks__rgtbtn);
        var toMove          = this.props.slideMove;

        // slide info
        var slideElMargin = this.props.slideGutter; // slide margin-right value
        var slideCount    = slideEl.length; // no. of slides
        var slideWidth    = (slidesContainer.width() - ((this.props.slidesToShow - 1) * slideElMargin)) / this.props.slidesToShow; // width of each slide
        var slideHeight   = slideEl.height(); // height of each slide
        
        // set slideWidth
        slideEl.css('width', slideWidth);

        // set slider width
        // (n slides * slide width) + (n - 1 slides * slide margin)
        sliderEl.css({
            width: ((slideWidth * toMove) + (slideElMargin * (toMove - 1))),
            height: slideHeight
        });

        // set slide container width
        // (n slides) * (outer width of each slide)
        // assign negative margin left value to hide n prepended slides
        slidesContainer.css({
            width: (slideCount * (slideWidth + slideElMargin)),
            marginLeft: - ((slideWidth + slideElMargin) * toMove)
        });

        // get the last n slides in array and prepend to front of slide list
    	slideEl = slideEl.slice(Math.max(slideEl.length - toMove, 1))
        slideEl.prependTo(slidesContainer); // last 3

        this.progressSlider();
        this.setState({
            sliderInitialized: true,
        })
    }
    progressSlider() {
        this.timeout = setTimeout(function () {
            this.moveRight();
            this.progressSlider();
        }.bind(this), this.props.slideInterval);
    }
    moveLeft = () => {
        // elements
        var slidesContainer = $(this.refs.howworks__slides);
        var slideEl         = slidesContainer.children('.howworks__slide');
        var toMove          = this.props.slideMove;
        var slideSpeed        = this.props.slideSpeed;

        // slide info
		var slideCount      = slideEl.length;
		var slideElMargin = parseInt($(slideEl[0]).css('margin-right'), 10);
        var slideWidth = slideEl.width();

        // set left value to move slide leftwards
        // callback fn:
        // - prepend last 3 slides to the front of slide list
        // - clear left value after prepend so end order of slides = moved state
        slidesContainer.animate({
            left: + ((slideWidth + slideElMargin) * toMove)
        }, slideSpeed, function () {
        	slideEl = slideEl.slice(Math.max(slideEl.length - toMove, 1)) // get last 3 slides
            slideEl.prependTo(slidesContainer);
            slidesContainer.css('left', '');
        });

        let curSlidePos = this.state.slidePos - toMove;
        if (curSlidePos < 0) {
            curSlidePos = slideCount + curSlidePos;
        }
        this.setState({slidePos : curSlidePos});
    }
    moveRight = () => {
        // elements
		var slidesContainer = $(this.refs.howworks__slides);
		var slideEl         = slidesContainer.children('.howworks__slide');
        var toMove          = this.props.slideMove;
        var slideSpeed        = this.props.slideSpeed;

        // slide info
		var slideCount      = slideEl.length;
		var slideElMargin = parseInt($(slideEl[0]).css('margin-right'), 10);
        var slideWidth = slideEl.width();

        // set right value to move slide rightwards
        // callback fn:
        // - append first 3 slides to the end of slide list
        // - clear left value after prepend so end order of slides = moved state
        slidesContainer.animate({
            left: - ((slideWidth + slideElMargin) * toMove)
        }, slideSpeed, function () {
        	slideEl.splice(toMove); // get first 3 slides
            slideEl.appendTo(slidesContainer);
            slidesContainer.css('left', '');
        });

        let curSlidePos = this.state.slidePos + toMove;
        if (curSlidePos >= slideCount) {
            curSlidePos = curSlidePos - slideCount;
        }
        this.setState({slidePos : curSlidePos});
    }
    handleMouseEnter() {
        clearTimeout(this.timeout);
    }
    handleMouseLeave() {
        this.progressSlider();
    }
    createTempDesc(description){
        description = (description).replace(/\\n/g,"<br>");
        return {__html: description}
    }
    render(){
        var howworksData = this.props.data;
        if (howworksData) {
        	// get slides
            var howSlides = howworksData.map(function(hows, i){
                return (
                    <div className="howworks__slide" key={i} data-slide-id={i}>
                        <div className="howworks__card">
                            <img className="thumbnail" src={hows.image} alt=""/>
                            <div className="content">
                                <h3 className="title">{hows.title}</h3>
                                <p className="description" dangerouslySetInnerHTML={this.createTempDesc(hows.description)}/>
                            </div>
                        </div>
                    </div>
                    )
            }, this);

			// get slide dots navigation
            var howDots = howworksData.map(function(hows, i){
            	var dotClass = 'dot';

            	if (i % this.props.slideMove == 0) {
                return (
                    <li className="howworks__dot" key={i} data-slide-id={i}>
                        <span className={this.state.slidePos === i ? dotClass + ' dot--active' : dotClass}>{i}</span>
                    </li>
                    )
            	}
            }, this);
        };
        return (
            <div className="howworks__slider" ref="howworks__slider" onMouseEnter={this.handleMouseEnter.bind(this)} onMouseLeave={this.handleMouseLeave.bind(this)}>
	            <div className="howworks__slidecontainer" ref="howworks__slidecontainer">
	                <div className="howworks__slides" ref="howworks__slides">
	                    {howSlides}
	                </div>
	            </div>
                <div className="howworks__slidernav">
	                <button
	                	className="howworks_angle howworks--prev"
	                	ref="howworks__lftbtn"
	                	onClick={this.moveLeft.bind(this)}>Prev</button>
	                <button
		                className="howworks_angle howworks--next"
		                ref="howworks__rgtbtn"
		                onClick={this.moveRight.bind(this)}>Next</button>
                </div>
                <ol className="howworks__dots">
                    {howDots}
                </ol>
            </div>
            )
    }
}
