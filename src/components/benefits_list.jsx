import React from 'react';

/**
 * Benefit item
 */
export default class BenefitList extends React.Component {
    render(){
        if (this.props.data) {
            var benefitlist = (this.props.data).map(function(benefit, i){
                return(
                    <BenefitItem
                    key={i}
                    icon={benefit.icon}
                    title={benefit.title}
                    description={benefit.description} />
                    )
            }, this);
        }
        return(
            <div className="benefit__list">
                {benefitlist}
            </div>
            )
    }
};

/**
 * Benefit Item
 */
class BenefitItem extends React.Component {
    createTempDesc(description){
        return {__html: description}
    }
    render(){
        var description = "";
        if (this.props.description) {
            description = (this.props.description).replace(/\\n/g,"<br>");
        }
        return(
            <div className="benefit__item">
                <div className={"benefit__icon icon icon--"+this.props.icon}></div>
                <div className="benefit__content">
                    <h3 className="benefit__title">{this.props.title}</h3>
                    <p className="benefit__desc" dangerouslySetInnerHTML={this.createTempDesc(description)}/>
                </div>
            </div>
            )
    }
}
