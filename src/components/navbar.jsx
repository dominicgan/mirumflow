import React from 'react';

/**
 * Navbar
 */
export default class NavBar extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            menuState: 'inactive'
        };
    }
    handleMobBtn(){
        var curState = this.state.menuState;
        if (this.state.menuState === 'active') {
            curState = 'inactive';
        } else if (this.state.menuState === 'inactive') {
            curState = 'active';
        }
        this.setState({
            menuState: curState
        });
    }
 	render(){
         var links        = this.props.menu;
         var socials      = this.props.social;
         var navMenuLinks = links.map(function(link, i) {
			// todo: set active class for first menu item
			var active = "";
			if (i == 0) {
				active = "active";
			}
			return (
				<MenuLink
				name={link}
				url="#"
				key={i}
				active={active}
				/>
				);
		}, this);
 		var navSocialLinks = socials.map(function(social, i) {
 			return (
 				<SocialLink
 				type={social.type}
 				url={social.url}
 				key={i}
 				/>
 				);
 		}, this);
 		return (
 			<nav className="navbar">
                <div className="navbar__content">
                    <div className="navbar__logo">Flow</div>
                    <div className="navbar__menu">
                        <button className="navbar__mob_btn" onClick={this.handleMobBtn.bind(this)}>MENU</button>
                        <ul className={this.state.menuState === "active" ? "menu menu--active" : "menu"}>
                            {navMenuLinks}
                        </ul>
                        <ul className="social">
                            {navSocialLinks}
                        </ul>
                    </div>
                </div>
 			</nav>
 			)
 	}
 };

/**
 * Navbar social media icon link
 */
class SocialLink extends React.Component {
 	render(){
 		return (
 			<li className="social__link">
     			<a
     			className={"social--"+ this.props.type}
     			href={this.props.url} title={this.props.type}>
     			{this.props.type}
     			</a>
 			</li>
 			)
 	}
}

/**
 * Navbar menu link
 */
class MenuLink extends React.Component {
    render(){
 		var classType = "menu__link";
 		if (this.props.active.length > 0) {
 			var classType = "menu__link link--"+this.props.active;
 		}
 		return (
 			<li className={classType}>
     			<a href={this.props.url}>{this.props.name}</a>
 			</li>
 			)
 	}
}
