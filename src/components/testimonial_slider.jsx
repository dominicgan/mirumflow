import React from 'react';
import $ from 'jquery';

/**
 * Testimonal slider component
 *
 * guides:
 * http://tech.oyster.com/using-react-and-jquery-together/
 * http://codepen.io/zuraizm/pen/vGDHl
 * http://codepen.io/brian-baum/pen/MwxorO.babel
 */
export default class TestimonialSlider extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            slidePos: 0,
            sliderInitialized: false,
            sliderActive: false
        };
    }
    componentDidMount(){
        if (this.props.data){
            this.initSlider();
        }
    }
    componentDidUpdate(){
        if (!this.state.sliderInitialized){
        this.initSlider();
        }
    }
    initSlider(){
        // elements
        var t_slidesContainer = $(this.refs.testi__slides);
        var t_sliderEl        = $(this.refs.testi__slidercontainer);
        var t_slideEl         = t_slidesContainer.children('.testimonial__slide');
        var t_slideLftBtn     = $(this.refs.testi__lftbtn);
        var t_slideRgtBtn     = $(this.refs.testi__rgtbtn);
        var t_toMove          = this.props.slideMove;

        // slide info
        var t_slideElMargin = this.props.slideGutter; // slide margin-right value
        var slideCount    = t_slideEl.length; // no. of slides
        var slideWidth    = (t_slidesContainer.width() - ((this.props.slidesToShow - 1) * t_slideElMargin)) / this.props.slidesToShow; // width of each slide
        var slideHeight   = t_slideEl.height(); // height of each slide

        // set slideWidth
        t_slideEl.css('width', slideWidth);

        // set slider width
        // (n slides * slide width) + (n - 1 slides * slide margin)
        t_sliderEl.css({
            width: ((slideWidth * t_toMove) + (t_slideElMargin * (t_toMove - 1))),
            height: slideHeight
        });

        // set slide container width
        // (n slides) * (outer width of each slide)
        // assign negative margin left value to hide n prepended slides
        t_slidesContainer.css({
            width: (slideCount * (slideWidth + t_slideElMargin)),
            marginLeft: - ((slideWidth + t_slideElMargin) * t_toMove)
        });

        // get the last n slides in array and prepend to front of slide list
        t_slideEl = t_slideEl.slice(Math.max(t_slideEl.length - t_toMove, 1))
        t_slideEl.prependTo(t_slidesContainer); // last 3

        this.progressSlider();
        this.setState({
            sliderInitialized: true,
        })
    }
    progressSlider() {
        this.timeout = setTimeout(function () {
            this.moveRight();
            this.progressSlider();
        }.bind(this), this.props.slideInterval);
    }
    moveLeft = () => {
        // elements
        var t_slidesContainer = $(this.refs.testi__slides);
        var t_slideEl         = t_slidesContainer.children('.testimonial__slide');
        var t_toMove          = this.props.slideMove;
        var slideSpeed        = this.props.slideSpeed;

        // slide info
        var slideCount      = t_slideEl.length;
        var t_slideElMargin = parseInt($(t_slideEl[0]).css('margin-right'), 10);
        var slideWidth = t_slideEl.width();

        // set left value to move slide leftwards
        // callback fn:
        // - prepend last 3 slides to the front of slide list
        // - clear left value after prepend so end order of slides = moved state
        t_slidesContainer.animate({
            left: + ((slideWidth + t_slideElMargin) * t_toMove),
            opacity: 0
        }, slideSpeed, function () {
            t_slideEl = t_slideEl.slice(Math.max(t_slideEl.length - t_toMove, 1)) // get last 3 slides
            t_slideEl.prependTo(t_slidesContainer);
            t_slidesContainer.css({
                'left': '',
                'opacity': 1,
            });
        });

        let curSlidePos = this.state.slidePos - t_toMove;
        if (curSlidePos < 0) {
            curSlidePos = slideCount + curSlidePos;
        }
        this.setState({slidePos : curSlidePos});
    }
    moveRight = () => {
        // elements
        var t_slidesContainer = $(this.refs.testi__slides);
        var t_slideEl         = t_slidesContainer.children('.testimonial__slide');
        var t_toMove          = this.props.slideMove;
        var slideSpeed        = this.props.slideSpeed;

        // slide info
        var slideCount      = t_slideEl.length;
        var t_slideElMargin = parseInt($(t_slideEl[0]).css('margin-right'), 10);
        var slideWidth = t_slideEl.width();

        // set right value to move slide rightwards
        // callback fn:
        // - append first 3 slides to the end of slide list
        // - clear left value after prepend so end order of slides = moved state
        t_slidesContainer.animate({
            left: - ((slideWidth + t_slideElMargin) * t_toMove),
            opacity: 0
        }, slideSpeed, function () {
            t_slideEl.splice(t_toMove); // get first 3 slides
            t_slideEl.appendTo(t_slidesContainer);
            t_slidesContainer.css({
                'left': '',
                'opacity': 0,
            }).animate({
            	opacity: 1
            }, slideSpeed);
        });

        let curSlidePos = this.state.slidePos + t_toMove;
        if (curSlidePos >= slideCount) {
            curSlidePos = curSlidePos - slideCount;
        }
        this.setState({slidePos : curSlidePos});
    }
    handleMouseEnter() {
        clearTimeout(this.timeout);
    }
    handleMouseLeave() {
        this.progressSlider();
    }
	render(){
		var testimonialsData = this.props.data;
		if (testimonialsData) {
			var testimonials = testimonialsData.map(function(testi, i){
				return (
					<div className="testimonial__slide" key={i} data-slide-id={i}>
						<blockquote className="testimonial__title">{testi.text}</blockquote>
						<hr className="testimonial__hr"/>
						<div className="testimonial__meta">
							<span className="testimonial__author">{testi.name}</span>
							<span className="testimonial__role">{testi.role}</span>
						</div>
					</div>
					)
			}, this);
		};
		return (
			<div className="testimonial__slider" ref="testi__slider" onMouseEnter={this.handleMouseEnter.bind(this)} onMouseLeave={this.handleMouseLeave.bind(this)}>
                <div className="testimonial__slidecontainer" ref="testi__slidecontainer">
                    <div className="testimonial__slides" ref="testi__slides">
                    {testimonials}
                    </div>
                </div>
	 			<TestimonialSliderDots data={testimonialsData} slidePos={this.state.slidePos}/>
			</div>
			)
	}
}

/**
 * Testimonal slider dots component (with avatar images)
 */
class TestimonialSliderDots extends React.Component {
	render(){
		var testimonialsData = this.props.data;
		if (testimonialsData) {
			var testiAvatars = testimonialsData.map(function(testi, i){
                var dotClass = 'testimonial__dot';

				return (
					<div className={this.props.slidePos === i ? dotClass + ' dot--active' : dotClass} key={i} data-slide-id={i}>
                        <img className="avatar" src={testi.avatar} alt={testi.name}/>
					</div>
					)
			}, this);
		}
		return(
				<div className="testimonial__dots">
				{testiAvatars}
				</div>
			)
	}
}
