import React from 'react';
import SectionHeader from '../components/sectionheader';
import TestimonialSlider from '../components/testimonial_slider';

/**
 * What they say section
 */
const WhatTheySay = React.createClass({
    render: function(){
        var h_title      = this.props.data.heading;
        var h_subtitle   = this.props.data.subtitle;
        var testimonials = this.props.data.testimonials;
        return (
            <section className="section section--white section--whattheysay" id="what-they-say">
                <div className="section__content">
                    <SectionHeader header={h_title} subtitle={h_subtitle}/>
                    <TestimonialSlider data={testimonials} slideSpeed={400} slidesToShow={1} slideGutter={0} slideMove={1} slideInterval={4000}/>
                </div>
            </section>
            )
    }
});

export default WhatTheySay;
