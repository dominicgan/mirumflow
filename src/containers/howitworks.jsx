import React from 'react';
import $ from 'jquery';
import SectionHeader from '../components/sectionheader';
import HowWorksSlider from '../components/howitworks_slider';

/**
 * How it works section
 */
const HowItWorks = React.createClass({
    render: function(){
        var h_title    = this.props.data.heading;
        var h_subtitle = this.props.data.subtitle;
        var slidesToShow = 3;

        let wWidth = $(window).width();
        if (wWidth > 959) {
            slidesToShow = 3;
        } else if (wWidth < 960 && wWidth > 639) {
            slidesToShow = 2;
        } else if (wWidth < 640) {
            slidesToShow = 1;
        }
        return (
            <section className="section section--red section--howitworks" id="how-it-works">
                <div className="section__content">
                    <SectionHeader header={h_title} subtitle={h_subtitle}/>
                    <HowWorksSlider data={this.props.data.slider} slideSpeed={1000} slidesToShow={slidesToShow} slideGutter={20} slideMove={3} slideInterval={4000}/>
                </div>
            </section>
            )
    }
});

export default HowItWorks;
