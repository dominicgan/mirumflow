import React from 'react';
import SectionHeader from '../components/sectionheader';
import BenefitList from '../components/benefits_list';

/**
 * Why use us section
 */
const WhyUseUs = React.createClass({
    render: function(){
        var h_title      = this.props.data.heading;
        var h_subtitle   = this.props.data.subtitle;
        var benefitsData = this.props.data.benefits;

        return (
            <section className="section section--white section--whyuseus" id="why-use-us">
                <div className="section__content">
                    <SectionHeader header={h_title} subtitle={h_subtitle}/>
                    <BenefitList data={benefitsData} />
                </div>
            </section>
            )
    }
});

export default WhyUseUs;
