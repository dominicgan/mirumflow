import React from 'react';
import SectionHeader from '../components/sectionheader';
import ContactForm from '../components/contactform';

/**
 * Get in touch section
 */
const GetInTouch = React.createClass({
    render: function(){
        var h_title    = this.props.data.heading;
        var h_subtitle = this.props.data.subtitle;
        return (
            <section className="section section--red section--getintouch" id="get-in-touch">
                <div className="section__content">
                    <SectionHeader header={h_title} subtitle={h_subtitle}/>
                    <ContactForm />
                </div>
            </section>
            )
    }
});

export default GetInTouch;
