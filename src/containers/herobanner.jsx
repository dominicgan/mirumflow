import React from 'react';
import SectionHeader from '../components/sectionheader';

/**
 * Hero section
 */
const HeroBanner = React.createClass({
    createSubtitleTemp(subtitle){
        return {__html: subtitle} // temp hack, to replace.
    },
    render: function(){
        var h_title      = this.props.data.heading;
        var h_subtitle   = this.props.data.subtitle;
        var deviceimg_xs    = this.props.data['screenshot-xs'];
        var deviceimg_sm    = this.props.data['screenshot-sm'];
        var deviceimg_md    = this.props.data['screenshot-md'];
        var deviceimg_lg    = this.props.data.screenshot;
        var appstoredata = this.props.data.appstore;

        if (h_subtitle) {
            h_subtitle = h_subtitle.replace(/\\n/g,"<br>");
        }

        if (appstoredata) {
            var appstorelinks = appstoredata.map(function(link, i){
                return (
                        <li className={"hero__applink hero--app-"+link.type} key={i}>
                            <a href={link.url} target="_blank">{link.type}</a>
                        </li>
                    )
            });
        }
        return (
            <section className="section section--hero" id="hero-banner">
                <div className="section__content">
                    <div className="hero__devicemock">
                        <img sizes="" srcSet={deviceimg_sm+' 300w, '+deviceimg_md+' 600w, '+deviceimg_lg+' 1000w'} src={deviceimg_xs} alt="App Preview"/>
                    </div>
                    <div className="hero__content">
                        <h1 className="hero__title">{h_title}</h1>
                        <p className="hero__subtitle" dangerouslySetInnerHTML={this.createSubtitleTemp(h_subtitle)}></p>
                        <ul className="hero__cta">{appstorelinks}</ul>
                    </div>
                </div>
            </section>
            )
    }
});

export default HeroBanner;
