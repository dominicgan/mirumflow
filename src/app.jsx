import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

// navbar + containers
import NavBar from './components/navbar';
import HeroBanner from './containers/herobanner';
import WhyUseUs from './containers/whyuseus';
import HowItWorks from './containers/howitworks';
import WhatTheySay from './containers/whattheysay';
import GetInTouch from './containers/getintouch';

// styles
import 'reset-css/reset.css';
import './scss/style.scss';

/**
 * Flow App
 */
var FlowApp = React.createClass({
    getInitialState: function() {
        return {
            menu       : [],
            social     : [],
            hero       : [],
            whyuseus   : [],
            howitworks : [],
            whattheysay: [],
            getintouch : []
        };
    },
    componentDidMount: function() {
        this.loadDataFromServer();
    },
    handleChange: function(i, props, curSel){
    },
    loadDataFromServer: function() {
        // get data.json from file
        var srcUrl = this.props.url;
        $.ajax({
            url: srcUrl,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({
                    menu       : data.site_info.pages,
                    social     : data.site_info.social,
                    hero       : data.sections["hero"],
                    whyuseus   : data.sections["why-us"],
                    howitworks : data.sections["how-it-works"],
                    whattheysay: data.sections["what-they-say"],
                    getintouch : data.sections["get-in-touch"]
                });
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this )
        });
    },
    render: function(){
        return (
            <div className="flow-app">
                <NavBar menu={this.state.menu} social={this.state.social} />
                <HeroBanner data={this.state.hero}/>
                <WhyUseUs   data={this.state.whyuseus}/>
                <HowItWorks data={this.state.howitworks}/>
                <WhatTheySay data={this.state.whattheysay}/>
                <GetInTouch data={this.state.getintouch}/>
            </div>
            );
    }
});

ReactDOM.render(
    <FlowApp url="data/app.json"/>
    , document.getElementById('app')
);
