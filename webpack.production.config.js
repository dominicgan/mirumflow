/**
 * Webpack config file for production
 *
 * @author Dominic Gan <dominicgangx@gmail.com>
 */
var path = require("path");
var fs   = require('fs');

var pkg = JSON.parse(fs.readFileSync('./build-config.json'));

var environments = require('gulp-environments');
var development  = environments.development;
var production   = environments.production;

var webpack           = require('webpack');
var extractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer      = require('autoprefixer');
var precss            = require('precss');

var extractCSS = new extractTextPlugin(pkg.build.css + '[name].css');

module.exports = {
	entry: {
		app: [
		path.join(process.cwd(), pkg.src.js, 'app.jsx')
		],
	},
	output: {
		path: path.join(process.cwd(), pkg.build.base),
		publicPath: '/',
		filename: 'js/[name].js'
	},
	plugins: [
	new webpack.DefinePlugin({
	  'process.env': {
	    NODE_ENV: JSON.stringify('production')
	  }
	}),
	new webpack.optimize.DedupePlugin(),
	new webpack.optimize.UglifyJsPlugin(),
	new webpack.optimize.OccurenceOrderPlugin(),
	new webpack.NoErrorsPlugin(),
	new webpack.HotModuleReplacementPlugin(),
	extractCSS
	],
	resolve: {
		extensions: ['', '.js', '.jsx']
	},
	debug: environments.development(),
	module: {
		loaders: [
		{
			// jsx loader
			test: /\.jsx?$/,
			exclude: /(node_modules|bower_components)/,
			include: [path.join(process.cwd(), pkg.src.js)],
			loaders: [
			'react-hot',
			'babel?presets[]=react,presets[]=es2015,presets[]=stage-1',
			'webpack-module-hot-accept'
			],
		},
		{
			// image loader
			test: /\.(jpe?g|png|gif|svg)$/i,
			exclude: /(node_modules|bower_components)/,
			include: [path.join(process.cwd(), pkg.src.img)],
			loaders: [
			'file?publicPath=../&hash=sha512&digest=hex&name='+pkg.build.img+'[hash].[ext]',
			'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
			]
		},
		{
				test: /\.css$/i,
				loader:
					extractCSS.extract(
						"style-loader",
						"css-loader?importLoaders=1!postcss-loader"
					)
			},
			{
				test: /\.scss$/,
				loader:
					extractCSS.extract(
						"style-loader",
						"css-loader?importLoaders=1!postcss-loader!sass-loader"
						)
			}
		],
	},
	postcss: function(){
		return [autoprefixer, precss];
	},
	imageWebpackLoader: {
		mozjpeg: {
			quality: 65
		},
		pngquant:{
			quality: "65-90",
			speed: 4
		},
		svgo:{
			plugins: [
			{
				removeViewBox: false
			},
			{
				removeEmptyAttrs: false
			}
			]
		}
	}
};
